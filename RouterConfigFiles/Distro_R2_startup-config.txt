!
version 15.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Distro_R2
!
!
!
!
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
license udi pid CISCO2911/K9 sn FTX152456Q0-
!
!
!
!
!
!
!
!
!
!
!
spanning-tree mode pvst
!
!
!
!
!
!
interface GigabitEthernet0/0
 ip address 192.168.20.1 255.255.255.0
 duplex auto
 speed auto
!
interface GigabitEthernet0/1
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface GigabitEthernet0/2
 no ip address
 duplex auto
 speed auto
 shutdown
!
interface GigabitEthernet0/0/0
 ip address 172.17.1.2 255.255.255.248
!
interface GigabitEthernet0/1/0
 ip address 172.17.2.2 255.255.255.248
!
interface GigabitEthernet0/2/0
 no ip address
 shutdown
!
interface GigabitEthernet0/3/0
 no ip address
 shutdown
!
interface Vlan1
 no ip address
 shutdown
!
router rip
 version 2
 network 172.17.0.0
 network 192.168.20.0
 no auto-summary
!
ip classless
!
ip flow-export version 9
!
!
!
!
!
!
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end

