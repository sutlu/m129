# Cisco CLI

[TOC]

## 1. Einleitung
Dies ist eine kleine Projektarbeit im Rahmen des Moduls m129 an der Technischen Berufsschule Zürich. Ich habe mir die Aufgabe gestellt einen Einstieg in die Dokumentation von Cisco ios, wie es im Packettracer als Router OS verwendet wird, zu wagen. Mein Ziel ist es ein Kleines Netzwerk Router-seitig komplett über das CLI zu konfigurieren. 

## 2. Beweggründe 
In grossen Netzen werden Router fast ausschliesslich über eine CLI konfiguriert. Dies geschieht aber oft nicht direkt sonder über eine Konfigurations-Datei. Um so eine Datei erstellen zu können, muss man das CLI des Routers aber verstehen und beherrschen. Ebenfalls bin ich der Ansicht, dass man in einer Konsole wesentlich effizienter arbeiten kann als mit einem GUI. Natürlich darf man das nicht generalisieren, aber im Falle einer Router Konfiguration oder z.B. die eines Linux Servers, ist über reine Texteingabe einfach schneller.

## 3. Logisches Layout

![Logisches Layout](img/LogischesLayout.png)
- Die Roten Linien sind 1000BASE-LX/LH Verbindungen
- Die Schwarzen Linien von Router zu Switch sind 1000BASE-T
- Die Schwarzen Linien von Switch zu Client sind 100BASE-T

## 4. Netzwerkadressierung

| Name              | Netz            | Subnetzmaske    |
|-------------------|-----------------|-----------------|
| Core              | 10.0.0.0        | 255.255.255.248 |
| Distribution 1    | 172.16.1.0      | 255.255.255.248 |
| Distribution 2    | 172.16.2.0      | 255.255.255.248 |
| Distribution 3    | 172.17.1.0      | 255.255.255.248 |
| Distribution 4    | 172.17.2.0      | 255.255.255.248 |
| Distribution 5    | 172.18.1.0      | 255.255.255.248 |
| Distribution 6    | 172.18.2.0      | 255.255.255.248 |
| Access 1          | 192.168.10.0    | 255.255.255.0   |
| Access 2          | 192.168.20.0    | 255.255.255.0   |
| Access 3          | 192.168.30.0    | 255.255.255.0   |

Das Netz wird in drei Stufen aufgeteilt: 
1. Zuoberst haben wir die beiden Core Router. Sie dienen zum einen als Verbindung zu je einem ISP, sind aber gleichzeitig auch dazu gedacht das ganze Netz etwas resilienter zu gestalten. Es kann einer der Beiden ausfallen und alle Clients hätten immer noch eine Verbindung zu einander. Es können auch einzelne Links zwischen den Routern down gehen und alle Clients haben untereinander trotzdem eine Verbindung.
2. Auf der mittleren Stufe befinden sich die distributions Router Sie bilden das Bindungsglied zwischen dem Core und der Access Stufe. Sie haben jeweils einen Link zu beiden Core Routern. 
3. Auf der Access Stufe wird jeweils ein Switch eingesetzt, um alle Clients mit dem Distributionsrouter zu verbinden.

Natürlich ist im Packettracer nur ein kleiner teil der Clients Abgebildet. Wenn diese Topologie in der Realen Welt eingesetz wird, hangen hinter jedem Distributionsrouter mehrere Switches und hinter jedem dieser Switches mehrere Clients.

## 5. Dokumentation CLI

### 5.1 Grundlegende Commands

Nach dem start des Routers muss man execution privileges erhalten:

```enable```

Man sieht das es geklappt hat wenn nun statt einem ">" ein "#" nach dem Hostnamen steht.

Mit folgendem Command wechselt man nun in das configuration Terminal:

```configuration terminal``` 

oder die Kurzform:

```conf t```

Nun kann dir Running Config bearbeitet werden.

Mit folgendem Command kann man sich diverse Konfigurationen anzeigen lassen:

```show <args>```

Hier steht das args für die jeweilige Konfiguration.
Ein paar Beispiele:

```show run``` zeig die Running Config
```show ip int brief``` zeig alle Interfaces und deren Konfiguration
```show ip route``` gibt die Routing Tabelle aus

Wenn man ```show ?``` eingibt werden alle möglichen Argumente, welche mit dem show command aufgerufen werden können ausgegeben. Dies gild nicht nur für show. Immer wenn ein "?" eingetippt wird, werden alle Möglichkeiten, die man hat aufgelistet.

Mit dem Command ```exit``` kommt man jeweils eine Ebene zurück. Also zum Beispiel wenn man ein Interface konfiguriert hat, kommt man zurück in das Configuration Terminal.

Mit dem Command ```end``` kommt man zurück in den Exec mode.

Falls bei der Konfiguration einen Fehler gemacht wurde kann man einfach eine eingabe mit eineim "no" vor dem selben Command rückgängig machen. Also zum Beispiel:

```no ip route 10.0.0.0 255.0.0.0 20.0.0.0```

Zum Schluss ist es wichtig zu wissen, dass wir immer nur die Running Config bearbeiten. Das heisst, wenn der Router neu gestartet wird sind alle Änderungen verloren. Also speichern wir die Running Config in die Startup Config in dem wir im Exec mode mit folgendem Command ausführen:

```write```

### 5.2 Grundkonfiguration auf jedem Router

Begeben Sie sich also in das konfigurations Terminal.

Zuerst setzen wir aber einmal einen Hostnamen mit folgendem command: 

```hostname <name>```

Wie Sie sehen hat sich der Hostname geändert und wir sind immer noch im konfigurations Modus. Deshalb fahren wir gleich fort, und können auf den jeweiligen Interfaces die IP-Adressen konfigurieren.
Mit folgendem Command wählt man das Interface aus:

```interface gigabitEthernet <interface number>```

oder die Kurzform:
```int gi <interface number>```

Hier gilt darauf zu achten, dass nicht alle Interfaces zwingend Gigabit Ethernet sein müssen. Es kann auch FastEthernet Interfaces oder andere haben.

Ist das Interface aus gewählt wir mit folgendem Command die IP-Adresse konfiguriert:

```ip addr <ip-address> <subnet mask>```

### 5.3 Routing

Im folgenden Abschnitt schauen wir zwei Möglichkeiten an, wie man Netzwerke mit Routern verbinden kann. Eine Dynamische Methode und Statisches Routing. In meinem Beispiel habe ich nur RIP angewendet. Dies hat den Grund, dass in einem redundanten Netz statische Routen wenig Sinn ergeben. Der vorteil von Dynamischen Routing ist, dass der Router merkt, wenn ein Link down geht. So kann auf eine andere Route ausweichen. Wenn Routen statisch gesetzt werden ist es dem Router egal ob sein Gegenstück schon lange nichts mehr gesendet hat. Ebenfalls hat dynamisches Routing den Vorteil, dass wesentlich weniger manuell eingegeben werden muss.

#### 5.3.1 RIP

Um ein RIP Routing einzurichten begeben wir uns zuerst in das Configuration Terminal. Danach brauchen wir folgenen Command:

```router rip```

Danach müssen wir dem Router die direkt angeschlossenen Netze angeben. Oder mindestens diejenigen, die Sie durch den Rest des Netzwerks propagiert haben wollen. Dies geschieht folgendermassen:

```network <network-ip>```

Also Beispielsweise:

```network 192.168.10.0```

Zu einem Späteren Zeitpunkt legen wir die Gateways of Last Resort für das Netz fest. Dies geschieht auf den beiden Core Routern und wird über Statisches Routing gemacht. Dass diese Routen aber auch durch das Netz propagiert werden muss noch folgende commands ausgeführt werden:

```version 2```

```default-information originate```

#### 5.3.2 Statisches Routing

Das Statische Routing ist in meinen Augen wesentlich komplizierter als Dynamisches, da man alle Routen die ein Router braucht manuell eintragen muss und über welches Gegenstück er in dieses Netz kommt. Um eine Statisch Route zu definieren brauchen wir folgenden Command:

```ip route <network-ip> <subnet-mask> <next-hop>```

Hier ein Beispiel: 

```ip route 192.168.10.0 255.255.255.0 10.0.0.1```

Wir sagen hier dem Router, dass das Netz 192.168.10.0/24 über den Router mit der IP-Adresse 10.0.0.1 erreichbar ist. Er schickt also alle Pakete mit der einer Destination-IP im Netz 192.168.10.0/24 an den Router mit der IP 10.0.0.1.

Bei den Core Routern wurde noch jeweils eine Statische Route zum Gateway of last Resort (dem jeweiligen ISP-Router) eingetragen. So kann mit einer Einstellung im RIP der Gateway of last Resort durch das Netz propagiert werden.

Mit diesem Wissen können alle Router konfiguriert werden, so dass alle Clients eine Verbindung untereinander haben und auch "ins Internet" kommen.

##  6. Konfiguration der einzelnen Router

Hier ein Beispiel einer Startupconfig eines Core Routers weiter unten im Dokument sind alle weiteren Startupconfig-Files verlinkt.

```
!
version 12.2
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Core_R1
!
!
!
!
!
!
!
!
no ip cef
no ipv6 cef
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
!
interface GigabitEthernet0/0
 ip address 10.0.0.1 255.255.255.248
!
interface GigabitEthernet1/0
 ip address 172.16.1.1 255.255.255.248
!
interface GigabitEthernet2/0
 ip address 172.17.1.1 255.255.255.248
!
interface GigabitEthernet3/0
 ip address 172.18.1.1 255.255.255.248
!
interface GigabitEthernet4/0
 ip address 32.32.32.33 255.255.255.0
!
router rip
 version 2
 network 10.0.0.0
 network 32.0.0.0
 network 172.16.0.0
 network 172.17.0.0
 network 172.18.0.0
 default-information originate
 no auto-summary
!
ip classless
ip route 0.0.0.0 0.0.0.0 32.32.32.32 
!
ip flow-export version 9
!
!
!
!
!
!
!
!
line con 0
!
line aux 0
!
line vty 0 4
 login
!
!
!
end
```

[Config Core_R2](RouterConfigFiles/Core_R2_startup-config.txt)  
[Config Distro_R1](RouterConfigFiles/Distro_R1_startup-config.txt)  
[Config Distro_R2](RouterConfigFiles/Distro_R2_startup-config.txt)  
[Config Distro_R3](RouterConfigFiles/Distro_R3_startup-config.txt)  

## 7. Fazit

Ich bin zufrieden mit Meiner Arbeit. Ich habe einiges Wissen über Cisco Geräte und deren Konfiguration festigen und erweitern können. Vorallem das RIP Routing hat mich fasziniert und ich denke ich habe ein Spannendes Netz gebaut. Leider war die Zeit am Schluss zu Kurz das Netz auf Redundanz komplett zu Testen. Ein Hinweis darauf, dass es aber funktionieren könnte ist, dass Wenn man einer der ISP-Router pingt, das Packet einmal über Core-R1 und das nächste über Core-R2 das Netz versuch zu verlassen. Natürlich kommt immer nur einmal Antwort. Ich habe versucht ein kleines zusätzliches Netz zu Bauen wofür aber eben die Zeit nicht mehr gereicht hat.
Mit Der Aufgabe Cisco CLI Commands zu dokumentieren bin ich ebenfalls zufrieden, auch wenn es noch ein Wenig Potential hat. IPv6 Varianten habe ich z.B. komplett weggelassen. Trotzdem ist die Liste nicht klein und für ein einfaches kleines Netz reicht es.